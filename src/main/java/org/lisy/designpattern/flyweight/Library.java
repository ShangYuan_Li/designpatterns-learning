package org.lisy.designpattern.flyweight;

import java.util.HashMap;
import java.util.Map;

/**
 * 享元工厂类
 * 
 * @author lisy
 */
public class Library {
	
	/**
	 * 缓存存放共享对象
	 */
    private Map<String, Book> bookPools = new HashMap<String, Book>();
	
    /**
     * 单例模式工厂
     */
    private static Library library = new Library();
    public static Library getInstance() {
        return library;
    }

    public Book libToBorrow(String name) {
    	Book order = null;
    	if (bookPools.containsKey(name)) {
    		order = bookPools.get(name);
    	} else {
    		order = new ConcreteBook(name);
    		bookPools.put(name, order);
    	}
    	return order;
    }
    
    /**
     * 统计对象个数
     */
    public int getBookSize() {
    	return bookPools.size();
    }
    
}
