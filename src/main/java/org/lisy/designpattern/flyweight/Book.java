package org.lisy.designpattern.flyweight;

/**
 * 抽象享元类
 * 
 * @author lisy
 */
public abstract class Book {

	public abstract void borrow(String date);
}
