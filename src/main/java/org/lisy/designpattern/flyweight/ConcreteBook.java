package org.lisy.designpattern.flyweight;

/**
 * 具体享元类
 * - 内部状态：不随外界环境改变而改变的共享部分(如：name)
 * - 外部状态：随着环境的改变而改变，不能够共享的状态(如：date)
 * 
 * @author lisy
 */
public class ConcreteBook extends Book {

	private String name;
	
	public ConcreteBook(String name) {
		this.name = name;
	}
	
	@Override
	public void borrow(String date) {
		System.out.println("borrow " + name + " " + date + "!");
	}

}
