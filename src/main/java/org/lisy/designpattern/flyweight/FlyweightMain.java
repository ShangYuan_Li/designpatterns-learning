package org.lisy.designpattern.flyweight;

/**
 * 享元模式
 * - 抽象享元(Flyweight)角色
 * - 具体享元(ConcreteFlyweight)角色
 * - 享元工厂(FlyweightFactory)角色
 * 优缺点:
 * - 优点:减少对象的创建
 * - 缺点:提高了系统的复杂度
 * 
 * @author lisy
 */
public class FlyweightMain {

	public static void main(String[] args) {
		Library library = Library.getInstance();
		// 借4次书
		library.libToBorrow("java").borrow("2021-12-07");
		library.libToBorrow("c++").borrow("2021-12-07");
		library.libToBorrow("python").borrow("2021-12-07");
		library.libToBorrow("java").borrow("2021-12-08");
		// 书本数量
		System.out.println(library.getBookSize());
	}
}
