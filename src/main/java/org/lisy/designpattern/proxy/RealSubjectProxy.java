package org.lisy.designpattern.proxy;

/**
 * 代理角色(代理类)
 * - 静态代理
 * - 需要为每个接口实现一个代理类
 * 
 * @author lisy
 */
public class RealSubjectProxy implements Subject {

	private RealSubject realSubject;
	
	public RealSubjectProxy(RealSubject realSubject) {
		this.realSubject = realSubject;
	}
	
	public void connect() {
		System.out.println("connect");
	}
	
	public void log() {
		System.out.println("over, write log");
	}
	
	@Override
	public void doWork() {
		connect();
		realSubject.doWork();
		log();
	}

}
