package org.lisy.designpattern.proxy;

/**
 * 抽象主题
 * - 一般实现为接口，是对(被代理对象的)行为的抽象
 * 
 * @author lisy
 */
public interface Subject {
	public void doWork();
}
