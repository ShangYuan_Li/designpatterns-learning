package org.lisy.designpattern.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 代理角色(代理类)
 * - 动态代理
 * - 实现被代理对象接口和代理类的解耦
 * 
 * @author lisy
 */
public class RealSubjectProxyFactory {

	// 维护被代理对象
	private Object realSubject;
	
	public RealSubjectProxyFactory(Object realSubject) {
		this.realSubject = realSubject;
	}

	public Object getProxyInstance() {
		return Proxy.newProxyInstance(realSubject.getClass().getClassLoader(), 
				realSubject.getClass().getInterfaces(), 
				new InvocationHandler() {
					
					@Override
					public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
						System.out.println("proxy connect");
						Object invoke = method.invoke(realSubject, args);
						System.out.println("proxy write log");
						return invoke;
					}
				});
	}
	
}
