package org.lisy.designpattern.proxy;

import java.lang.reflect.Method;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

/**
 * 代理角色(代理类)
 * - cglib代理
 * - 第三方代码生成类库，运行时在内存中动态生成一个子类对象从而实现对目标对象功能的扩展
 * - 实现被代理对象接口和代理类的解耦
 * 
 * @author lisy
 */
public class RealSubjectProxyInterceptor implements MethodInterceptor{

	// 维护被代理对象
    private Object realSubject;
    
    public RealSubjectProxyInterceptor(Object realSubject) {
        this.realSubject = realSubject;
    }
    
    //为目标对象生成代理对象
    public Object getProxyInstance() {
        //工具类
        Enhancer en = new Enhancer();
        //设置父类
        en.setSuperclass(realSubject.getClass());
        //设置回调函数
        en.setCallback(this);
        //创建子类对象代理
        return en.create();
    }

	@Override
	public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
		System.out.println("cglib connect");
        // 执行目标对象的方法
        Object returnValue = method.invoke(realSubject, args);
        System.out.println("cglib write log");
		return returnValue;
	}
}
