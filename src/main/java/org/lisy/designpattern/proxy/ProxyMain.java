package org.lisy.designpattern.proxy;

/**
 * 代理模式
 * - 抽象类
 * - 具体实现类(被代理对象)
 * - 代理类
 * 
 * @author lisy
 */
public class ProxyMain {

	public static void main(String[] args) {
		
		// 静态代理
		new RealSubjectProxy(new RealSubject()).doWork();
		
		System.out.println("=====================");
		
		// 动态代理(例:aop)
		((Subject) new RealSubjectProxyFactory(new RealSubject()).getProxyInstance()).doWork();
		
		System.out.println("=====================");
		
		// cglib代理
		((Subject) new RealSubjectProxyInterceptor(new RealSubject()).getProxyInstance()).doWork();
	}
}
