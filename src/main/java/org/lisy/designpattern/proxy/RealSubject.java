package org.lisy.designpattern.proxy;

/**
 * 被代理角色(具体主题类)
 * 
 * @author lisy
 */
public class RealSubject implements Subject {

	@Override
	public void doWork() {
		System.out.println("Hello World!");
	}
	
}
