package org.lisy.designpattern.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * 抽象主题(被观察者)
 * 
 * @author lisy
 */
public abstract class Subject {
	
    protected List<Observer> observers = new ArrayList<Observer>();
    
    /**
     * 注册观察者
     */
    public void add(Observer observer) {
        observers.add(observer);
    }
    
    /**
     * 移除观察者
     */
    public void remove(Observer observer) {
        observers.remove(observer);
    }
    
    /**
     * 通知方法
     */
    public abstract void notifyObserver(); 
}
