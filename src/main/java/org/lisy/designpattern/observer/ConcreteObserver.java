package org.lisy.designpattern.observer;

/**
 * 具体观察者
 * 
 * @author lisy
 */
public class ConcreteObserver extends Observer{

	@Override
	public void response() {
        System.out.println("concrete observer receive!");
    }
	
}
