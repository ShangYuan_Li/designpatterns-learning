package org.lisy.designpattern.observer;

/**
 * 观察者模式
 * - 抽象主题(Subject)
 * - 具体主题(Concrete Subject)
 * - 抽象观察者(Observer)
 * - 具体观察者(Concrete Observer)
 * 优缺点:
 * - 优点:解耦，支持广播通讯
 * - 缺点:观察者多执行耗时，有循环调用风险，异步调用问题
 * 
 * @author lisy
 */
public class ObserverMain {

	public static void main(String[] args) {
        Observer obs = (Observer) new ConcreteObserver();
        Subject subject = new ConcreteSubject();
        subject.add(obs);
        subject.notifyObserver();
	}
}
