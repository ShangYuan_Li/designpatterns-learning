package org.lisy.designpattern.observer;

/**
 * 抽象观察者
 * 
 * @author lisy
 */
public abstract class Observer {
	
	public abstract void response();
}
