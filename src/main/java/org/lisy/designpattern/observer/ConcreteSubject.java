package org.lisy.designpattern.observer;

/**
 * 具体主题(具体被观察者)
 * 
 * @author lisy
 */
public class ConcreteSubject extends Subject {
	
    public void notifyObserver() {
        System.out.println("concrete subject change, notify observers...");
        for (Object obs : observers) {
            ((Observer) obs).response();
        }
    }
}
