package org.lisy.designpattern.builder;

/**
 * 抽象建造者
 * - 包含创建对象各个属性的抽象方法
 * - 包含返回对象方法
 * 
 * @author lisy
 */
public abstract class Builder {
	
	// 创建实体对象
    protected House house = new House();
	
	public abstract Builder buildWindow(String window);
	
	public abstract Builder buildDoor(String door);
	
	public abstract Builder buildWall(String wall);
	
	public House build() {
		return house;
	};
}
