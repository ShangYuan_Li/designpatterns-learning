package org.lisy.designpattern.builder;

/**
 * 建造者模式 - 对象属性较多时推荐使用
 * 方式一:
 * - 实体类
 * - 抽象建造者类
 * - 具体建造者类
 * 方式二:
 * - 实体类
 * - 实体内部静态类
 * 
 * @author lisy
 */
public class BuilderMain {

	public static void main(String[] args) {

		Builder builder = new ConcreteBuilder();
		House build = builder.buildWindow("builder windows!")
				.buildDoor("builder doors!")
				.buildWall("builder walls!")
				.build();
		System.out.println(build.toString());
		
		Person person = new Person.PersonBuilder()
                .basicInfo(1, "lishangyuan", 26)
                .score(90)
                .weight(60)
                .loc("zj", "902")
                .build();
		System.out.println(person.toString());
	}
}
