package org.lisy.designpattern.builder;

/**
 * 实体类
 * 
 * @author lisy
 */
public class House {

	private String window;
	
	private String door;
	
	private String wall;

	public String getWindow() {
		return window;
	}

	public void setWindow(String window) {
		this.window = window;
	}

	public String getDoor() {
		return door;
	}

	public void setDoor(String door) {
		this.door = door;
	}

	public String getWall() {
		return wall;
	}

	public void setWall(String wall) {
		this.wall = wall;
	}

	@Override
	public String toString() {
		return "House [window=" + window + ", door=" + door + ", wall=" + wall + "]";
	}
	
}
