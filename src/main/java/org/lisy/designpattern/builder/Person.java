package org.lisy.designpattern.builder;

/**
 * 实体类 - 使用lombok时可以直接使用@Builder注解
 * 
 * @author lisy
 */
public class Person {
	
	private int id;
	
	private String name;
	
	private int age;
	
	private double weight;
	
	private int score;
	
	private Location loc;

	private Person() {
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + ", age=" + age + ", weight=" + weight + ", score=" + score
				+ ", loc=" + loc.toString() + "]";
	}
	
	/**
	 * 内部静态类方式
	 * 
	 * @author lisy
	 */
	public static class PersonBuilder {
		Person p = new Person();

		public PersonBuilder basicInfo(int id, String name, int age) {
			p.id = id;
			p.name = name;
			p.age = age;
			return this;
		}

		public PersonBuilder weight(double weight) {
			p.weight = weight;
			return this;
		}

		public PersonBuilder score(int score) {
			p.score = score;
			return this;
		}

		public PersonBuilder loc(String street, String roomNo) {
			p.loc = new Location(street, roomNo);
			return this;
		}

		public Person build() {
			return p;
		}
	}
}

class Location {
	String street;
	String roomNo;

	public Location(String street, String roomNo) {
		this.street = street;
		this.roomNo = roomNo;
	}

	@Override
	public String toString() {
		return "Location [street=" + street + ", roomNo=" + roomNo + "]";
	}
	
}