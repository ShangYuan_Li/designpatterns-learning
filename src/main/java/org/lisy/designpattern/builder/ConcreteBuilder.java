package org.lisy.designpattern.builder;

/**
 * 具体建造者
 * 
 * @author lisy
 */
public class ConcreteBuilder extends Builder  {

	@Override
	public Builder buildWindow(String window) {
		house.setWindow(window);
		return this;
	}

	@Override
	public Builder buildDoor(String door) {
		house.setDoor(door);
		return this;
	}

	@Override
	public Builder buildWall(String wall) {
		house.setWall(wall);
		return this;
	}

}
