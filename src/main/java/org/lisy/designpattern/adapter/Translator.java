package org.lisy.designpattern.adapter;

/**
 * 目标接口
 * - 当前系统业务所期待的接口，它可以是抽象类或接口
 * 
 * @author lisy
 */
public interface Translator {
	
	public String translator();
}
