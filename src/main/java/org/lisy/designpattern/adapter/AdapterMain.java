package org.lisy.designpattern.adapter;

/**
 * 适配器模式
 * - 目标接口(Target)
 * - 适配者类(Adaptee)
 * - 适配器类(Adapter)
 * 优缺点
 * - 优点:目标类和适配者类解耦，方便复用、扩展
 * - 缺点:过多的使用适配器，会让系统非常零乱，不易整体进行把握
 * 
 * @author lisy
 */
public class AdapterMain {

	public static void main(String[] args) {
		Adapter adapter = new Adapter(new Speaker());
		System.out.println(adapter.translator());
	}
}
