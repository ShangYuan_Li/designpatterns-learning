package org.lisy.designpattern.adapter;

/**
 * 适配器类
 * - 充当转换器，通过继承或引用适配者的对象(需要定义)，把适配者接口转换成目标接口
 * - 让客户按目标接口的格式访问适配者
 * 
 * @author lisy
 */
public class Adapter implements Translator {
	
	private Speaker speaker;

	public Adapter(Speaker speaker) {
		this.speaker = speaker;
	}
	
	@Override
	public String translator() {	
		String speak = speaker.speak();
		return speak.toUpperCase();
	}

}
