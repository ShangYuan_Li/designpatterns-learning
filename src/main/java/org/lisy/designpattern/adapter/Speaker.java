package org.lisy.designpattern.adapter;

/**
 * 适配者类
 * - 被访问和适配的现存组件库中的组件接口
 * 
 * @author lisy
 */
public class Speaker {

	public String speak() {
		return "hello";
	}
}
