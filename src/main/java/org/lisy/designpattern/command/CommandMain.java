package org.lisy.designpattern.command;

/**
 * 命令模式
 * - 抽象命令类(Command)
 * - 具体命令类(ConcreteCommand)
 * - 实现者/接收者角色(Receiver)
 * - 调用者/请求者角色(Invoker)
 * 优缺点:
 * - 优点:发送者和接受者解耦，可以容易的设计一个宏命令
 * - 缺点:出现很多个具体的命令类，造成系统的冗余
 * 
 * @author lisy
 */
public class CommandMain {

	public static void main(String[] args) {
		
        Content content = new Content("Hello World!");
        Invoker invoker = new Invoker(new InsertCommand(content));
        invoker.call();
        invoker.bindCommand(new DeleteCommand(content)).call();
	}
}
