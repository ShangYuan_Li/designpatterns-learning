package org.lisy.designpattern.command;

/**
 * 具体命令类 - 新增
 * - 调用接收者对象的相关操作
 * 
 * @author lisy
 */
public class InsertCommand extends Command {
	
    Content content;
    
    public InsertCommand(Content content) {
        this.content = content;
    }

    @Override
    public void doit() {
    	content.setMsg(content.getMsg() + " insert!");
    	content.write();
    }
}
