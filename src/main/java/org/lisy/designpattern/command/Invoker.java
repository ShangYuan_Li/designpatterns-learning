package org.lisy.designpattern.command;

/**
 * 调用类
 * - 通过命令对象来执行请求
 * - 只与抽象命令类之间存在关联关系
 * 
 * @author lisy
 */
public class Invoker {
	
    private Command command;  
    
    //构造注入
    public Invoker(Command command) {  
        this.command = command;  
    }  
      
    //设值注入
    public Invoker bindCommand(Command command) {  
        this.command = command;  
        return this;
    }
      
    //业务方法，用于调用命令类的方法  
    public void call() {  
        command.doit();
    }
}
