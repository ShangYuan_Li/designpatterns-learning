package org.lisy.designpattern.command;

/**
 * 具体命令类 - 删除
 * 
 * @author lisy
 */
public class DeleteCommand extends Command {
	
    Content content;
    
    public DeleteCommand(Content content) {
        this.content = content;
    }

    @Override
    public void doit() {
    	content.setMsg(content.getMsg() + " delete!");
    	content.write();
    }
}
