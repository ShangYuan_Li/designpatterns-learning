package org.lisy.designpattern.command;

/**
 * 接收类
 * - 执行与请求相关的业务代码
 * 
 * @author lisy
 */
public class Content {
	
    private String msg;
    
    public Content(String msg) {
    	this.msg = msg;
    }

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public void write() {
		System.out.println(msg);
	}
    
}
