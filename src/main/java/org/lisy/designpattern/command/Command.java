package org.lisy.designpattern.command;

/**
 * 抽象命令类
 * 
 * @author lisy
 */
public abstract class Command {
	
    public abstract void doit();
}
