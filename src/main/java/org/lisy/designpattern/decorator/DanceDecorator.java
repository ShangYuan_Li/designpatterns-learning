package org.lisy.designpattern.decorator;

/**
 * 具体装饰类(ConcreteDecorator) - 跳舞
 * - 负责往被装饰对象添加额外的功能
 * 
 * @author lisy
 */
public class DanceDecorator extends Decorator {

	public DanceDecorator(Robot robot) {
		super(robot);
	}

	@Override
	public void doSomething() {
		robot.doSomething();
		System.out.println("跳舞");
	}

}
