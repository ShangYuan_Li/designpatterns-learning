package org.lisy.designpattern.decorator;

/**
 * 抽象装饰类(Decorator)
 * - 持有一个具体的被修饰对象
 * - 实现接口类继承的公共接口
 * 
 * @author lisy
 */
public abstract class Decorator implements Robot {

	public Robot robot;
	
	public Decorator(Robot robot) {
		this.robot = robot;
	}
	
}
