package org.lisy.designpattern.decorator;

/**
 * 对象接口(Component) - 机器人
 * - 定义装饰对象和具体对象的共同接口
 * 
 * @author lisy
 */
public interface Robot {

	void doSomething();
}
