package org.lisy.designpattern.decorator;

/**
 * 装饰器模式
 * - 对象接口
 * - 具体对象
 * - 抽象装饰类
 * - 具体装饰类
 * 优缺点:
 * - 优点：降低耦合度，装饰灵活
 * - 缺点：多层装饰较复杂
 * 
 * @author lisy
 */
public class DecoratorMain {

	public static void main(String[] args) {
		
		// 使用装饰类(例:IO流-InputStream)
		new DanceDecorator(new FirstRobot()).doSomething();
	}
}
