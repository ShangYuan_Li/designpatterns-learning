package org.lisy.designpattern.decorator;

/**
 * 具体对象(ConcreteComponent) - 第一代机器人
 * 
 * @author lisy
 */
public class FirstRobot implements Robot {

	@Override
	public void doSomething() {
		System.out.println("对话");
		System.out.println("唱歌");
	}

}
