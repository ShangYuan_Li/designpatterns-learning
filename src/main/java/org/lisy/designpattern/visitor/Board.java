package org.lisy.designpattern.visitor;

/**
 * 具体元素类 - 主板
 * 
 * @author lisy
 */
public class Board extends ComputerPart {

	@Override
	void accept(Visitor visitor) {
		visitor.visitBoard(this);
	}

	@Override
	double getPrice() {
		return 200;
	}
}
