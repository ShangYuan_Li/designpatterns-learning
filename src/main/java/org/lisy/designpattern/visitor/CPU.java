package org.lisy.designpattern.visitor;

/**
 * 具体元素类 - cpu
 * - 实现抽象元素角色提供的 accept() 操作，其方法体通常都是visitor.visit(this)
 * - 另外具体元素中可能还包含本身业务逻辑的相关操作
 * 
 * @author lisy
 */
public class CPU extends ComputerPart {

	@Override
	void accept(Visitor visitor) {
		visitor.visitCpu(this);
	}

	@Override
	double getPrice() {
		return 500;
	}
}
