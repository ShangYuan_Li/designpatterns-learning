package org.lisy.designpattern.visitor;

/**
 * 具体访问者 - 个人
 * - 确定访问者访问一个元素时该做什么
 * 
 * @author lisy
 */
public class PersonelVisitor implements Visitor {
	
	double totalPrice = 0.0;

	@Override
	public void visitCpu(CPU cpu) {
		totalPrice += cpu.getPrice() * 0.9;
	}

	@Override
	public void visitMemory(Memory memory) {
		totalPrice += memory.getPrice() * 0.85;
	}

	@Override
	public void visitBoard(Board board) {
		totalPrice += board.getPrice() * 0.95;
	}
}
