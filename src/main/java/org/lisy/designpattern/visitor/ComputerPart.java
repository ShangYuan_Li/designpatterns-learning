package org.lisy.designpattern.visitor;

/**
 * 抽象元素类 - 电脑组件
 * - 声明一个包含接受操作accept()的接口
 * - 被接受的访问者对象作为accept()方法的参数
 * 
 * @author lisy
 */
public abstract class ComputerPart {
	
	abstract void accept(Visitor visitor);

	abstract double getPrice();
}
