package org.lisy.designpattern.visitor;

/**
 * 抽象访问者
 * - 访问具体元素的接口，为每个具体元素类对应一个访问操作visit()
 * - 参数类型标识了被访问的具体元素
 * 
 * @author lisy
 */
public interface Visitor {

	void visitCpu(CPU cpu);

	void visitMemory(Memory memory);

	void visitBoard(Board board);
}
