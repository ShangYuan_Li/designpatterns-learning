package org.lisy.designpattern.visitor;

/**
 * 访问者模式
 * - 抽象访问者(Visitor)
 * - 具体访问者(ConcreteVisitor)
 * - 抽象元素类(Element)
 * - 具体元素类(ConcreteElement)
 * - 对象结构类(Object Structure)
 * 优缺点:
 * - 优点:访问者扩展性好，可复用，灵活将数据结构与操作解耦，单一职责
 * - 缺点:增加新的元素类很困难，破坏封装，违反依赖倒置原则-依赖了具体类，而不是抽象类
 * 
 * @author lisy
 */
public class VisitorMain {
	
	public static void main(String[] args) {
		
		Computer computer = new Computer();
		// 公司购买价格
		CorpVisitor corpVisitor = new CorpVisitor();
		computer.acccept(corpVisitor);
		System.out.println("CorpVisitor: " + corpVisitor.totalPrice);
		
		// 个人购买价格
		PersonelVisitor personelVisitor = new PersonelVisitor();
		computer.acccept(personelVisitor);
		System.out.println("PersonelVisitor: " + personelVisitor.totalPrice);
	}
	
}
