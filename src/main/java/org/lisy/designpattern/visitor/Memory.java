package org.lisy.designpattern.visitor;

/**
 * 具体元素类 - 内存
 * 
 * @author lisy
 */
public class Memory extends ComputerPart {

	@Override
	void accept(Visitor visitor) {
		visitor.visitMemory(this);
	}

	@Override
	double getPrice() {
		return 300;
	}
}
