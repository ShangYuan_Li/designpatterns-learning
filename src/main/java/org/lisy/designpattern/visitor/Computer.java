package org.lisy.designpattern.visitor;

import java.util.ArrayList;
import java.util.List;

/**
 * 对象结构类 - 电脑
 * - 包含元素角色的容器，提供让访问者对象遍历容器中的所有元素的方法
 * - 通常由 List、Set、Map 等聚合类实现
 * @author lisy
 */
public class Computer {

	private List<ComputerPart> list = new ArrayList<ComputerPart>();

	public Computer() {
		this.list.add(new CPU());
		this.list.add(new Memory());
		this.list.add(new Board());
	}
	
	public void acccept(Visitor visitor) {
		if (list != null && list.size() > 0) {
			for (ComputerPart computerPart : list) {
				computerPart.accept(visitor);
			}
		}
	}
}
