package org.lisy.designpattern.interpreter;

/**
 * 非终结符表达式 - 运算符的抽象父类
 * - 通常一个解释器对应一个语法规则，可以包含其它的解释器
 * - 可以有多种非终结符解释器
 * 
 * @author lisy
 */
public abstract class SymbolExpression implements Expression {

	protected Expression left;
	protected Expression right;

	public SymbolExpression(Expression left, Expression right) {
		this.left = left;
		this.right = right;
	}

}
