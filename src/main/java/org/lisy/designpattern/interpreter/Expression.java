package org.lisy.designpattern.interpreter;

import java.util.HashMap;

/**
 * 抽象解释器
 * 
 * @author lisy
 */
public interface Expression {
	
    public int interprete(HashMap<String, Integer> var);
}
