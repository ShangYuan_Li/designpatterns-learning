package org.lisy.designpattern.interpreter;

import java.util.HashMap;

/**
 * 终结符表达式 - 参加运算的元素对象
 * - 用来实现文法中和终结符相关的解释操作，不再包含其它的解释器
 * 
 * @author lisy
 */
public class VarExpression implements Expression {

	private String key;

	public VarExpression(String key) {
		this.key = key;
	}

	@Override
	public int interprete(HashMap<String, Integer> var) {
		return (Integer) var.get(this.key);
	}
}
