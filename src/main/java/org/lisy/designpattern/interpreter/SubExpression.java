package org.lisy.designpattern.interpreter;

import java.util.HashMap;

/**
 * 非终结符表达式 - 减法
 * 
 * @author lisy
 */
public class SubExpression extends SymbolExpression {
 
	public SubExpression(Expression left, Expression right) {
		super(left, right);
	}
 
	@Override
	public int interprete(HashMap<String, Integer> var) {
		return super.left.interprete(var) - super.right.interprete(var);
	}

}
