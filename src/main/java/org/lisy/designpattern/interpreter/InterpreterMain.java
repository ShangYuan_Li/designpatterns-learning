package org.lisy.designpattern.interpreter;

import java.util.HashMap;

/**
 * 解释器模式
 * - 抽象解释器(AbstractExpression)
 * - 终结符表达式(TerminalExpression)
 * - 非终结符表达式(NonterminalExpression)
 * - 上下文(Context)
 * 优缺点:
 * - 优点:扩展性强
 * - 缺点:会引起类膨胀；采用递归调用方法，将会导致调试非常复杂，影响效率
 * 
 * @author lisy
 */
public class InterpreterMain {

	public static void main(String[] args) {

		// 构造运算元素的值列表，HashMap代替Context
		HashMap<String, Integer> ctx = new HashMap<String, Integer>();
		ctx.put("a", 10);
		ctx.put("b", 20);
		ctx.put("c", 30);
		ctx.put("d", 40);
		ctx.put("e", 50);
		ctx.put("f", 60);

		Calculator calc = new Calculator("a+b-c");
		int result = calc.calculate(ctx);
		System.out.println("Result of a+b-c = " + result);

		calc = new Calculator("d-a-b+c");
		result = calc.calculate(ctx);
		System.out.println("Result of d-a-b+c = " + result);
	}
}
