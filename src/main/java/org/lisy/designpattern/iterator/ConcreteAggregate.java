package org.lisy.designpattern.iterator;

import java.util.ArrayList;
import java.util.List;

/**
 * 具体聚合类
 * 
 * @author lisy
 */
public class ConcreteAggregate implements Aggregate {

	private List<Object> list = new ArrayList<Object>();

	public void add(Object obj) {
		list.add(obj);
	}

	public Iterator iterator() {
		return new ConcreteIterator(list);
	}

	public void remove(Object obj) {
		list.remove(obj);
	}
}
