package org.lisy.designpattern.iterator;

/**
 * 迭代器模式 
 * - 抽象迭代器(Iterator) 
 * - 具体迭代器(Concretelterator) 
 * - 抽象聚合类(Aggregate) 
 * - 具体聚合类(ConcreteAggregate) 
 * 优缺点: 
 * - 优点: 简化了遍历方式，可以提供多种遍历方式，封装性良好
 * - 缺点: 使用迭代器方式遍历较为繁琐
 * 
 * @author lisy
 */
public class IteratorMain {

	public static void main(String[] args) {
		
		Aggregate aggregate = new ConcreteAggregate();
		aggregate.add("Collection");
		aggregate.add("List");
		aggregate.add("Set");
		aggregate.remove("List");
		Iterator iterator = aggregate.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}
}
