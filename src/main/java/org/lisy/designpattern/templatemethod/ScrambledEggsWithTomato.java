package org.lisy.designpattern.templatemethod;

/**
 * 具体实现类 - 西红柿炒鸡蛋
 * - 实现抽象类中所定义的抽象方法和钩子方法
 * 
 * @author lisy
 */
public class ScrambledEggsWithTomato extends Cooking {

    @Override
    public void step1() {
        System.out.println("put eggs and tomato, ");
    }

    @Override
	public void otherStep() {
        System.out.println("add a little sugar, ");
    }
}
