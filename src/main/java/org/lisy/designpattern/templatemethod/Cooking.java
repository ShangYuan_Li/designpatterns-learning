package org.lisy.designpattern.templatemethod;

/**
 * 抽象模板类 - 做饭
 * - 模板方法：定义了算法的骨架，按某种顺序调用其包含的基本方法
 * - 基本方法包含以下几种类型:
 * 	 抽象方法：在抽象类中声明，由具体子类实现
 *   具体方法：在抽象类中已经实现，在具体子类中可以继承或重写它
 *   钩子方法：在抽象类中已经实现，包括用于判断的逻辑方法和需要子类重写的空方法两种
 * 
 * @author lisy
 */
public abstract class Cooking {
	
	/**
	 * 抽象方法
	 */
	abstract void step1();
	
	/**
	 * 具体方法
	 */
	public void step2() {
		System.out.println("add a little salt, ");
	};
    
    /**
     * 钩子方法
     */
    public void otherStep() {
    	
    }
	
    /**
     * 模板方法
     */
    public void cook() {
    	System.out.println("start cooking, ");
    	step1();
    	step2();
    	otherStep();
    	System.out.println("complete!");
    }
}
