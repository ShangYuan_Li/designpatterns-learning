package org.lisy.designpattern.templatemethod;

/**
 * 模板方法
 * - 抽象模板类
 * - 具体实现类
 * 优缺点:
 * - 优点:扩展，复用
 * - 缺点:父类加抽象方法所有子类都需要更改，增加子类系统复杂
 * 
 * @author lisy
 */
public class TemplateMethodMain {

	public static void main(String[] args) {
		Cooking cooking = new ScrambledEggsWithTomato();
		cooking.cook();
	}
}
