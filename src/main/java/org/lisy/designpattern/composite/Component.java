package org.lisy.designpattern.composite;

/**
 * 抽象容器
 * 
 * @author lisy
 */
public abstract class Component {

	/**
	 * 添加成员 - 叶子对象则抛异常
	 */
    public void addComponent(Component component) {
    	throw new UnsupportedOperationException("leaf unsupported");
    }
    
    /**
	 * 移除成员 - 叶子对象则抛异常
	 */
    public void removeComponent(Component component) {
    	throw new UnsupportedOperationException("leaf unsupported");
    };
    
    /**
	 * 获取容器 - 叶子对象则抛异常
	 */
    public Component getComponent(int i) {
    	throw new UnsupportedOperationException("leaf unsupported");
    };
    
    /**
   	 * 业务方法
   	 */
    public void operation() {
    	operation(null);
    };
    
    public abstract void operation(String name);
}
