package org.lisy.designpattern.composite;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.ObjectUtils;

/**
 * 子容器 - 文件夹
 * 
 * @author lisy
 */
public class Composite extends Component {

	private String name;
	 
    public Composite(String name) {
        this.name = name;
    }
	
	private List<Component> list = new ArrayList<Component>();

	@Override
	public void addComponent(Component component) {
		list.add(component);
	}

	@Override
	public void removeComponent(Component component) {
		list.remove(component);
	}

	@Override
	public Component getComponent(int i) {
		Component component = list.get(i);
		return component;
	}
	
	@Override
	public void operation(String path) {
		for (Component component : list) {
			if (ObjectUtils.isEmpty(path)) {
				component.operation(name);
			} else {
				component.operation(path +"/" + name);
			}
		}
	}

}
