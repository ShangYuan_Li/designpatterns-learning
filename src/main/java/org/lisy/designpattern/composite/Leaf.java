package org.lisy.designpattern.composite;

import org.springframework.util.ObjectUtils;

/**
 * 叶子类 - 文件
 * 
 * @author lisy
 */
public class Leaf extends Component {
	
    private String name;
 
    public Leaf(String name) {
        this.name = name;
    }
    
	@Override
	public void operation(String path) {
		if (ObjectUtils.isEmpty(path)) {
			System.out.println(name);
		} else {
			System.out.println(path + "/" + name);
		}
	}

}
