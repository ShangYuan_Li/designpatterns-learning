package org.lisy.designpattern.composite;

/**
 * 组合模式
 * - 抽象组件(Component)角色
 * - 组合对象(Composite)角色
 * - 叶子对象(Leaf)角色
 * 优缺点:
 * - 优点:代码和容器解耦，方便扩展新构件
 * - 缺点:系统设计复杂
 * 
 * @author lisy
 */
public class CompositeMain {

	public static void main(String[] args) {
		
		// 创建文件夹
		Composite folder = new Composite("folder");
		Composite imageFolder = new Composite("imageFolder");
		Composite jpgImageFolder = new Composite("jpgImageFolder");
		folder.addComponent(imageFolder);
		imageFolder.addComponent(jpgImageFolder);
		
		// 创建文件
		Leaf exe = new Leaf("file.exe");
		Leaf file = new Leaf("file.txt");
		folder.addComponent(file);
		Leaf pngFile = new Leaf("file.png");
		imageFolder.addComponent(pngFile);
		Leaf jpgFile = new Leaf("file.jpg");
		jpgImageFolder.addComponent(jpgFile);
		
		try {
			// 业务调用 - 获取文件路径
			exe.operation();
			folder.operation();
			// 异常测试
			file.addComponent(jpgFile);
		} catch (UnsupportedOperationException e) {
			System.out.println(e.toString());
		}
	}

}
