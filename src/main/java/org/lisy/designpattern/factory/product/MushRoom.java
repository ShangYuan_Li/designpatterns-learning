package org.lisy.designpattern.factory.product;

/**
 * 具体产品类 - 蘑菇
 * 
 * @author lisy
 */
public class MushRoom extends AbstractFoodProduct{
    
	@Override
	public void getFoodName() {
		System.out.println("eat mushroom");
	}
}
