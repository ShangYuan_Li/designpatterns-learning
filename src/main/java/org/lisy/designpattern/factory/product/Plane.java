package org.lisy.designpattern.factory.product;

/**
 * 具体产品类 - 飞机
 * 
 * @author lisy
 */
public class Plane extends AbstractMoveProduct {
    
	public void go() {
        System.out.println("plane flying shushua....");
    }
    
}
