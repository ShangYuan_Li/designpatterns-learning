package org.lisy.designpattern.factory.product;

/**
 * 抽象产品类-武器
 * 
 * @author lisy
 */
public abstract class AbstractWeaponProduct {

	public abstract void shoot();
	
}
