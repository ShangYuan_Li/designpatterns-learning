package org.lisy.designpattern.factory.product;

/**
 * 具体产品类 - 魔法棒
 * 
 * @author lisy
 */
public class MagicStick extends AbstractWeaponProduct{
	
    public void shoot() {
        System.out.println("magic-stick diandian....");
    }
    
}
