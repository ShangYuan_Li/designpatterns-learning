package org.lisy.designpattern.factory.product;

/**
 * 具体产品类 - 突击枪
 * 
 * @author lisy
 */
public class AK47 extends AbstractWeaponProduct{
	
    public void shoot() {
        System.out.println("AK47 tututututu....");
    }
    
}
