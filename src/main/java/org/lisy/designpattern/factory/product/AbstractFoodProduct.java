package org.lisy.designpattern.factory.product;

/**
 * 抽象产品类-食物
 * 
 * @author lisy
 */
public abstract class AbstractFoodProduct {

	public abstract void getFoodName();
	
}
