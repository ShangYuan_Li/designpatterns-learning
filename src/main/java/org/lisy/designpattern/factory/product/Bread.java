package org.lisy.designpattern.factory.product;

/**
 * 具体产品类 - 面包
 * 
 * @author lisy
 */
public class Bread extends AbstractFoodProduct{

	@Override
	public void getFoodName() {
		System.out.println("eat bread");
	}
}
