package org.lisy.designpattern.factory.product;

/**
 * 具体产品类 - 汽车
 * 
 * @author lisy
 */
public class Car extends AbstractMoveProduct {
	
    public void go() {
        System.out.println("car go wuwuwuwuw....");
    }
    
}
