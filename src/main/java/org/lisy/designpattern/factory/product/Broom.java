package org.lisy.designpattern.factory.product;

/**
 * 具体产品类 - 扫帚
 * 
 * @author lisy
 */
public class Broom extends AbstractMoveProduct{

    @Override
    public void go() {
        System.out.println("broom flying chuachuachua .....");
    }
}
