package org.lisy.designpattern.factory.product;

/**
 * 抽象产品类-出行工具
 * 
 * @author lisy
 */
public abstract class AbstractMoveProduct {
	
    public abstract void go();
    
}
