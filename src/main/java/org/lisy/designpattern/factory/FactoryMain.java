package org.lisy.designpattern.factory;

import org.lisy.designpattern.factory.abstractfactory.AbstractClanFactory;
import org.lisy.designpattern.factory.abstractfactory.MagicFactory;
import org.lisy.designpattern.factory.abstractfactory.ModernFactory;
import org.lisy.designpattern.factory.factorymethod.AbstractMoveFactory;
import org.lisy.designpattern.factory.factorymethod.MoveFactory;
import org.lisy.designpattern.factory.product.Broom;
import org.lisy.designpattern.factory.product.Car;
import org.lisy.designpattern.factory.product.Plane;
import org.lisy.designpattern.factory.simplefactory.SimpleFactory;

/**
 * 工厂系列模式
 * - 简单工厂
 * - 工厂方法
 * - 抽象工厂
 * 
 * @author lisy
 */
public class FactoryMain {
    public static void main(String[] args) {
        
        // 简单工厂 - 静态工厂方法(例:DateFormat)
        new SimpleFactory().create("car").go();
        new SimpleFactory().create("broom").go();
        new SimpleFactory().create("plane").go();
        
        System.out.println("====================");
        
        // 工厂方法(例:Collection)
        AbstractMoveFactory factory = new MoveFactory();
        factory.create(Car.class).go();
        factory.create(Broom.class).go();
        factory.create(Plane.class).go();
        
        System.out.println("====================");
        
        // 抽象工厂
        AbstractClanFactory modernFactory = new ModernFactory();
        modernFactory.createMove().go();
        modernFactory.createWeapon().shoot();
        modernFactory.createFood().getFoodName();
        
        AbstractClanFactory magicFactory = new MagicFactory();
        magicFactory.createMove().go();
        magicFactory.createWeapon().shoot();
        magicFactory.createFood().getFoodName();
    }
}
