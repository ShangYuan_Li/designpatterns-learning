package org.lisy.designpattern.factory.abstractfactory;

import org.lisy.designpattern.factory.product.AbstractFoodProduct;
import org.lisy.designpattern.factory.product.AbstractMoveProduct;
import org.lisy.designpattern.factory.product.AbstractWeaponProduct;
import org.lisy.designpattern.factory.product.Broom;
import org.lisy.designpattern.factory.product.MagicStick;
import org.lisy.designpattern.factory.product.MushRoom;

/**
 * 具体工厂实现 - 魔法
 * 
 * @author lisy
 */
public class MagicFactory extends AbstractClanFactory {

	@Override
	public AbstractMoveProduct createMove() {
		return new Broom();
	}

	@Override
	public AbstractFoodProduct createFood() {
		return new MushRoom();
	}

	@Override
	public AbstractWeaponProduct createWeapon() {
		return new MagicStick();
	}
}
