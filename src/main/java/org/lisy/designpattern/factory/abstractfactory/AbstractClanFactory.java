package org.lisy.designpattern.factory.abstractfactory;

import org.lisy.designpattern.factory.product.AbstractFoodProduct;
import org.lisy.designpattern.factory.product.AbstractMoveProduct;
import org.lisy.designpattern.factory.product.AbstractWeaponProduct;

/**
 * 抽象工厂类
 * - 优点：方便产品族扩展
 * - 缺点：不利于产品等级结构扩展
 * 
 * @author lisy
 */
public abstract class AbstractClanFactory {
	
	public abstract AbstractMoveProduct createMove();
	
	public abstract AbstractFoodProduct createFood();
    
	public abstract AbstractWeaponProduct createWeapon();
	
}
