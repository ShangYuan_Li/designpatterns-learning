package org.lisy.designpattern.factory.abstractfactory;

import org.lisy.designpattern.factory.product.AK47;
import org.lisy.designpattern.factory.product.AbstractFoodProduct;
import org.lisy.designpattern.factory.product.AbstractMoveProduct;
import org.lisy.designpattern.factory.product.AbstractWeaponProduct;
import org.lisy.designpattern.factory.product.Bread;
import org.lisy.designpattern.factory.product.Car;

/**
 * 具体工厂实现 - 现代
 * 
 * @author lisy
 */
public class ModernFactory extends AbstractClanFactory {

	@Override
	public AbstractMoveProduct createMove() {
		return new Car();
	}

	@Override
	public AbstractFoodProduct createFood() {
		return new Bread();
	}

	@Override
	public AbstractWeaponProduct createWeapon() {
		return new AK47();
	}

}
