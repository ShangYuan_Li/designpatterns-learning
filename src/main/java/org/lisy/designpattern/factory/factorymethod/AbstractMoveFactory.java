package org.lisy.designpattern.factory.factorymethod;

import org.lisy.designpattern.factory.product.AbstractMoveProduct;

/**
 * 抽象工厂类
 * 
 * @author lisy
 */
public abstract class AbstractMoveFactory {
	
	public abstract <T> AbstractMoveProduct create(Class<T> c);
	
}
