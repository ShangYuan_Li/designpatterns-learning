package org.lisy.designpattern.factory.factorymethod;

import org.lisy.designpattern.factory.product.AbstractMoveProduct;

/**
 * 具体工厂类
 * - 优点：方便产品等级结构扩展
 * - 缺点：一个抽象产品类，一个工厂实现一种产品族
 * @author lisy
 */
public class MoveFactory extends AbstractMoveFactory{

	@Override
	public <T> AbstractMoveProduct create(Class<T> c) {
		AbstractMoveProduct instance = null;
		try {
			instance = (AbstractMoveProduct) Class.forName(c.getName()).getDeclaredConstructor().newInstance();
		} catch (Exception e) {
			System.out.println(c.getName() + "-工厂创建失败！");
			return null;
		}
		return instance;
	}


}
