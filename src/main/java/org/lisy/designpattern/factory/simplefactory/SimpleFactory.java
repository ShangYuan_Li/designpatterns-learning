package org.lisy.designpattern.factory.simplefactory;

import org.lisy.designpattern.factory.product.AbstractMoveProduct;
import org.lisy.designpattern.factory.product.Broom;
import org.lisy.designpattern.factory.product.Car;
import org.lisy.designpattern.factory.product.Plane;

/**
 * 简单工厂 
 * - 优点：传入一个参数就可以创建想要的对象 
 * - 缺点：可扩展性不好，新增产品种类需要更改工厂类代码
 * 
 * @author lisy
 */
public class SimpleFactory {

	public AbstractMoveProduct create(String type) {
		if (type == null) {
			return null;
		}
		if (type.equalsIgnoreCase("CAR")) {
			return new Car();
		} else if (type.equalsIgnoreCase("PLANE")) {
			return new Plane();
		} else if (type.equalsIgnoreCase("BROOM")) {
			return new Broom();
		}
		return null;
	}

}
