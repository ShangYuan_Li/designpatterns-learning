package org.lisy.designpattern.strategy;

/**
 * 抽象策略类 - 抽象比较策略
 * 
 * @author lisy
 */
@FunctionalInterface
public interface Comparator<T> {
	
    int compare(T o1, T o2);
}
