package org.lisy.designpattern.strategy;

/**
 * 策略实现类 - 比较高度
 * 
 * @author lisy
 */
public class CatHeightComparator implements Comparator<Cat> {
	
    @Override
    public int compare(Cat o1, Cat o2) {
        if (o1.height > o2.height) {
        	return -1;
        } else if (o1.height < o2.height) {
        	return 1;
        } else {
        	return 0;
        }
    }
    
}
