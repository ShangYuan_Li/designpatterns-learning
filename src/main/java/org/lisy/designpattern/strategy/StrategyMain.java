package org.lisy.designpattern.strategy;

import java.util.Arrays;

/**
 * 策略模式
 * - context 策略调用类-比较
 * - 抽象策略类
 * - 策略实现类
 * @author lisy
 */
public class StrategyMain {
	
	public static void main(String[] args) {
		Cat[] cat = { new Cat(3, 2), new Cat(5, 3), new Cat(1, 6) };
		Sorter<Cat> sorter = new Sorter<Cat>();
		sorter.sort(cat, new CatHeightComparator());
		System.out.println(Arrays.toString(cat));
		sorter.sort(cat, new CatWeightComparator());
		System.out.println(Arrays.toString(cat));

		// 实现新策略 - 比较体重
		sorter.sort(cat, (o1, o2) -> {
			if (o1.weight > o2.weight) {
	        	return -1;
	        } else if (o1.weight < o2.weight) {
	        	return 1;
	        } else {
	        	return 0;
	        }	
		});
		System.out.println(Arrays.toString(cat));
	}
}
