package org.lisy.designpattern.facade;

/**
 * 子系统角色(SubSystem)  - 电视
 * 
 * @author lisy
 */
public class TV {
	
	public void open(){
        System.out.println("TV has been opened!");
    }
	
}
