package org.lisy.designpattern.facade;

/**
 * 子系统角色(SubSystem)  - 灯
 * 
 * @author lisy
 */
public class Light {
	
	public void open(){
        System.out.println("Light has been opened!");
    }
	
}
