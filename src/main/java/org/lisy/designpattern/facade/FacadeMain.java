package org.lisy.designpattern.facade;

/**
 * 外观模式
 * - 外观角色
 * - 子系统角色(多个)
 * 优缺点:
 * - 优点：减小系统依赖，提高安全性，提高灵活性
 * - 缺点：不符合开闭原则，无法修改子系统
 * 
 * @author lisy
 */
public class FacadeMain {
	
	public static void main(String[] args) {
		
		// 一步操作完成准备工作
		Facade facade = new Facade();
		facade.open();
	}
    
}
