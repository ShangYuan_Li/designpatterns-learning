package org.lisy.designpattern.facade;

/**
 * 子系统角色(SubSystem)  - 加热器
 * 
 * @author lisy
 */
public class Heater {
	
	public void open(){
        System.out.println("Heater has been opened!");
    }
	
}
