package org.lisy.designpattern.singleton;

/**
 * lazy loading 懒汉式
 * 双重检查方式：
 * - 虽然达到了按需初始化的目的，但却带来线程不安全的问题
 * - 可以通过getInstance加synchronized解决，但也带来效率下降
 * - 妄图通过减小同步代码块的方式提高效率，然后不可行
 * - 加入双重检查和volatile
 */
public class LazilySingletonCheck {
	
	/**
	 * volatile 防止本地化优化(JIT)时，指令会重排序
	 */
    private static volatile LazilySingletonCheck INSTANCE; 

    private LazilySingletonCheck() {
    }

    public static LazilySingletonCheck getInstance() {
        if (INSTANCE == null) {
            //双重检查
            synchronized (LazilySingletonCheck.class) {
                if(INSTANCE == null) {
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    INSTANCE = new LazilySingletonCheck();
                }
            }
        }
        return INSTANCE;
    }

    public void m() {
        System.out.println("m");
    }
    
    /**
     * 防止序列化破坏实例
     * @return
     */
    private Object readResolve() {
    	return INSTANCE;
    }

    public static void main(String[] args) {
        for(int i=0; i<100; i++) {
            new Thread(()->{
                System.out.println(LazilySingletonCheck.getInstance().hashCode());
            }).start();
        }
    }
}
