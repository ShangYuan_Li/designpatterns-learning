package org.lisy.designpattern.singleton;

/**
 * 枚举单例
 * - 解决线程同步
 * - 没有构造方法，可以防止反序列化(反射)
 */
public enum EnumSingleton {

    INSTANCE;

    public void m() {}

    public static void main(String[] args) {
        for(int i=0; i<100; i++) {
            new Thread(()->{
                System.out.println(EnumSingleton.INSTANCE.hashCode());
            }).start();
        }
    }

}
