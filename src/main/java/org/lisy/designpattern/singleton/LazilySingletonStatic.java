package org.lisy.designpattern.singleton;

/**
 * lazy loading 懒汉式
 * 静态内部类方式：
 * - JVM保证单例
 * - 加载外部类时不会加载内部类，这样可以实现懒加载
 */
public class LazilySingletonStatic {

    private LazilySingletonStatic() {
    }

    private static class LazilySingletonStaticHolder {
    	private final static LazilySingletonStatic INSTANCE = new LazilySingletonStatic();
    }

    public static LazilySingletonStatic getInstance() {
        return LazilySingletonStaticHolder.INSTANCE;
    }

    public void m() {
        System.out.println("m");
    }

    public static void main(String[] args) {
        for(int i=0; i<100; i++) {
            new Thread(()->{
                System.out.println(LazilySingletonStatic.getInstance().hashCode());
            }).start();
        }
    }

}
