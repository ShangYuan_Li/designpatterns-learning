package org.lisy.designpattern.singleton;

/**
 * 饿汉式(推荐)
 * 类加载到内存后，就实例化一个单例，JVM保证线程安全
 * 唯一缺点：不管用到与否，类装载时就完成实例化
 * Class.forName("")
 */
public class EarlySingleton {
    
	// 实例化方式一
	private static final EarlySingleton INSTANCE = new EarlySingleton();

    // 实例化方式二
    /*
    private static final Mgr01 INSTANCE;
    static {
        INSTANCE = new Mgr01();
    }*/
    
    private EarlySingleton() {};

    public static EarlySingleton getInstance() {
        return INSTANCE;
    }

    public void m() {
        System.out.println("m");
    }

    public static void main(String[] args) {
        EarlySingleton m1 = EarlySingleton.getInstance();
        EarlySingleton m2 = EarlySingleton.getInstance();
        System.out.println(m1 == m2);
    }
}
