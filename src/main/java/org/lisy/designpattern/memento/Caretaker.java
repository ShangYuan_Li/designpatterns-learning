package org.lisy.designpattern.memento;

import java.util.ArrayList;
import java.util.List;

/**
 * 管理者
 * - 对备忘录进行管理，提供保存与获取备忘录的功能
 * 
 * @author lisy
 */
public class Caretaker {
	
	private List<Memento> mementoList = new ArrayList<Memento>();
	
	private int current = 0; 

	public void setMemento(Memento memento) {
		mementoList.add(memento);
		current++;
	}

	public Memento getMemento() {
		if (mementoList.size() > 0) {
			if (current > 1) {
				current--;
			}
			return mementoList.get(current-1);
		}
		return new Memento(null);
	}

	public Memento getNextMemento() {
		if (mementoList.size() > 0) {
			if (current < mementoList.size()) {
				current++;
			}
			return mementoList.get(current-1);
		}
		return new Memento(null);
	}
}
