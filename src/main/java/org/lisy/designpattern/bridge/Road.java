package org.lisy.designpattern.bridge;

/**
 * 抽象类 - 公路
 * - 定义维护了一个实现类接口类型的对象，具有关联关系
 * 
 * @author lisy
 */
public abstract class Road {
	
	protected Vehicle vehicle;

    public Road(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public abstract void driveOnRoad();
}
