package org.lisy.designpattern.bridge;

/**
 * 抽象类扩充类 - 水泥路
 * 
 * @author lisy
 */
public class CementRoad extends Road {
	
	public CementRoad(Vehicle vehicle) {
        super(vehicle);
    }

    @Override
    public void driveOnRoad() {
        super.vehicle.drive();
        System.out.println("drive cementRoad ！");
    }
}
