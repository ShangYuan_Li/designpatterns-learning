package org.lisy.designpattern.bridge;

/**
 * 抽象类扩充类 - 石子路
 * - 可以调用在具体实现类中的业务方法
 * 
 * @author lisy
 */
public class UnpavedRoad extends Road {
	
	public UnpavedRoad(Vehicle vehicle) {
        super(vehicle);
    }

    @Override
    public void driveOnRoad() {
        super.vehicle.drive();
        System.out.println("drive UnpavedRoad ！");
    }
}
