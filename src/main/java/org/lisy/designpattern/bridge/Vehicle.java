package org.lisy.designpattern.bridge;

/**
 * 实现类接口 - 运输工具
 * - 提供基本操作
 * 
 * @author lisy
 */
public interface Vehicle {
	
	public void drive();
}
