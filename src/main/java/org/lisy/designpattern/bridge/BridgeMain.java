package org.lisy.designpattern.bridge;

/**
 * 桥接模式 - 解决2层继承问题
 * - 抽象类(Abstraction)
 * - 扩充抽象类(RefinedAbstraction)
 * - 实现类接口(Implementor)
 * - 具体实现类(ConcreteImplementor)
 * 优缺点
 * - 优点:抽象和实现分离，可扩展，对用户隐藏细节
 * - 缺点:识别两个独立变化的维度，增加系统设计和理解难度
 * 
 * @author lisy
 */
public class BridgeMain {

	public static void main(String[] args) {
		
		// 向扩充抽象类注册具体实现类，调用业务方法
		new CementRoad(new Car()).driveOnRoad();
	}
}
