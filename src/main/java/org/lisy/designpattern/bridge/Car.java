package org.lisy.designpattern.bridge;

/**
 * 具体实现类 - 小汽车
 * - 提供给抽象类具体的业务操作方法
 * 
 * @author lisy
 */
public class Car implements Vehicle {

	@Override
	public void drive() {
		System.out.print("Car ");
	}
}
