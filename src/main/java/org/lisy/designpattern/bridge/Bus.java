package org.lisy.designpattern.bridge;

/**
 * 具体实现类 - 公共汽车
 * - 提供给抽象类具体的业务操作方法
 * 
 * @author lisy
 */
public class Bus implements Vehicle{

	@Override
	public void drive() {
		System.out.print("Bus ");
	}
}
