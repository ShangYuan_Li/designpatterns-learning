package org.lisy.designpattern.prototype;

import java.io.Serializable;

/**
 * 具体原型 - 印章
 * 
 * @author lisy
 */
public class Seal implements Serializable, Cloneable, Prototype {

	private static final long serialVersionUID = 1L;

	/**
	 * 印章名称
	 */
	private String name;
	/**
	 * 已知图片
	 */
	private Image image;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "Seal{" + "name='" + name + '\'' + ", image=" + image + '}';
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public Prototype getShallowCloneInstance() throws CloneNotSupportedException {
		return (Prototype) clone();
	}

	@Override
	public Prototype getDeepCloneInstance(Prototype prototype) {
		return PrototypeUtil.getSerializInstance(prototype);
	}

}