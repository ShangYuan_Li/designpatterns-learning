package org.lisy.designpattern.prototype;

/**
 * 抽象原型
 * 
 * @author lisy
 */
public interface Prototype {
	
    /**
     * 获取浅克隆对象 - 引用数据类型，指向同一个地址值
     */
    Prototype getShallowCloneInstance() throws CloneNotSupportedException;
    
    /**
     * 获取深克隆对象
     */
    Prototype getDeepCloneInstance(Prototype prototype);
    
}
