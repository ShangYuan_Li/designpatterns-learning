package org.lisy.designpattern.prototype;

import java.io.Serializable;

/**
 * 具体原型 - 发票
 * 
 * @author lisy
 */
public class Invoice implements Cloneable, Serializable, Prototype {

	private static final long serialVersionUID = 1L;

	/**
	 * 票头
	 */
	private String ticketHeader;
	/**
	 * 票号
	 */
	private int ticketNo;
	/**
	 * 发票联名称
	 */
	private String name;
	/**
	 * 发票联颜色
	 */
	private String color;
	/**
	 * 发票联公司
	 */
	private String company;
	/**
	 * 公司印章
	 */
	private Seal companySeal;
	/**
	 * 监制印章
	 */
	private Seal supervisedSeal;
	/**
	 * 是否有效
	 */
	private Boolean effective;

	public String getTicketHeader() {
		return ticketHeader;
	}

	public void setTicketHeader(String ticketHeader) {
		this.ticketHeader = ticketHeader;
	}

	public int getTicketNo() {
		return ticketNo;
	}

	public void setTicketNo(int ticketNo) {
		this.ticketNo = ticketNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Seal getCompanySeal() {
		return companySeal;
	}

	public void setCompanySeal(Seal companySeal) {
		this.companySeal = companySeal;
	}

	public Seal getSupervisedSeal() {
		return supervisedSeal;
	}

	public void setSupervisedSeal(Seal supervisedSeal) {
		this.supervisedSeal = supervisedSeal;
	}

	public Boolean getEffective() {
		return effective;
	}

	public void setEffective(Boolean effective) {
		this.effective = effective;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public String toString() {
		return "Invoice{" + "ticketHeader='" + ticketHeader + '\'' + ", ticketNo=" + ticketNo + ", name='" + name + '\''
				+ ", color='" + color + '\'' + ", company='" + company + '\'' + ", companySeal=" + companySeal
				+ ", supervisedSeal=" + supervisedSeal + ", effective=" + effective + '}';
	}

	@Override
	public Prototype getShallowCloneInstance() throws CloneNotSupportedException {
		return (Prototype) clone();
	}

	@Override
	public Prototype getDeepCloneInstance(Prototype prototype) {
		return PrototypeUtil.getSerializInstance(prototype);
	}

}