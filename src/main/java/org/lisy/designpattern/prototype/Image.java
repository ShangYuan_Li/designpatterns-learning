package org.lisy.designpattern.prototype;

import java.io.Serializable;

/**
 * 具体原型 - 图片
 * 
 * @author lisy
 */
public class Image implements Prototype, Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	/**
	 * 颜色
	 */
	private String color = "red";
	/**
	 * 高度
	 */
	private Integer height = 10;
	/**
	 * 宽度
	 */
	private Integer width = 8;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	@Override
	public String toString() {
		return "Image{" + "color='" + color + '\'' + ", height=" + height + ", width=" + width + '}';
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public Prototype getShallowCloneInstance() throws CloneNotSupportedException {
		return (Prototype) clone();
	}

	@Override
	public Prototype getDeepCloneInstance(Prototype prototype) {
		return PrototypeUtil.getSerializInstance(prototype);
	}

}
