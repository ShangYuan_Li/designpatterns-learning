package org.lisy.designpattern.prototype;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * 原型工具类
 * 
 * @author lisy
 */
public class PrototypeUtil {

	/**
	 * 通过序列化获取一个深度克隆的对象
	 */
	public static Prototype getSerializInstance(Prototype prototype) {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(prototype);
			ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bis);
			Prototype copy = (Prototype) ois.readObject();
			bos.flush();
			bos.close();
			ois.close();
			return copy;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("深度拷贝异常！" + e.toString());
		}
		return null;
	}

}
