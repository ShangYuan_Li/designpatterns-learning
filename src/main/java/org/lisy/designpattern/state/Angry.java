package org.lisy.designpattern.state;

/**
 * 具体状态类 - 生气
 * 
 * @author lisy
 */
public class Angry extends State{

	@Override
	void doWork() {
		System.out.println("working negative !");
	}

}
