package org.lisy.designpattern.state;

/**
 * 环境类 - 人
 * - 维护一个抽象状态类State的实例
 * 
 * @author lisy
 */
public class Persion {
	
	private State state;

	public Persion changesState(State state) {
		this.state = state;
		return this;
	}
	
	public void work() {
		state.doWork();
	}
}
