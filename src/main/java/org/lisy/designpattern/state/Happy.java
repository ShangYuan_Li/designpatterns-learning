package org.lisy.designpattern.state;

/**
 * 具体状态类 - 开心
 * 
 * @author lisy
 */
public class Happy extends State{

	@Override
	void doWork() {
		System.out.println("working actively !");
	}

}
