package org.lisy.designpattern.state;

/**
 * 状态模式
 * - 环境角色
 * - 状态抽象类
 * - 具体状态类(多个)
 * 优缺点:
 * - 优点:结构清晰(避免switch和if)，封装，扩展
 * - 缺点:子类会太多
 * 
 * @author lisy
 */
public class StateMain {

	public static void main(String[] args) {
		
		Persion persion = new Persion();
		persion.changesState(new Happy()).work();
		persion.changesState(new Angry()).work();
		persion.changesState(new Sad()).work();
	}
	
}
