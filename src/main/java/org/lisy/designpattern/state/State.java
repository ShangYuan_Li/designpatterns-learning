package org.lisy.designpattern.state;

/**
 * 状态抽象类
 * 
 * @author lisy
 */
public abstract class State {

	abstract void doWork();
}
