package org.lisy.designpattern.state;

/**
 * 具体状态类 - 悲伤
 * 
 * @author lisy
 */
public class Sad  extends State{

	@Override
	void doWork() {
		System.out.println("no working !");
	}

}