package org.lisy.designpattern.mediator;

/**
 * 抽象同事类
 * 
 * @author lisy
 */
public abstract class Colleague {

	protected String name;
	
	protected Mediator mediator;
	
    public Colleague(String name, Mediator mediator) {
    	this.name = name;
        this.mediator = mediator;
    }
    
}
