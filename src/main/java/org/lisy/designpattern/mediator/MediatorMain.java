package org.lisy.designpattern.mediator;

/**
 * 中介者模式
 * - 抽象同事类
 * - 具体同事类(多个)
 * - 抽象中介类
 * - 具体中介类
 * 
 * @author lisy
 */
public class MediatorMain {

	public static void main(String[] args) {
		
		// 创建中介者
		ConcreteMediator mediator = new ConcreteMediator();
		// 注册中介者
        ConcreteColleagueOne colleagueOne = new ConcreteColleagueOne("同事一", mediator);
        ConcreteColleagueTwo colleagueTwo = new ConcreteColleagueTwo("同事二", mediator);
        // 注册同事类
        mediator.setColleagueOne(colleagueOne);
        mediator.setColleagueTwo(colleagueTwo);
        // 发消息
        colleagueOne.send("同事一", "Nice to meet u.");
        colleagueTwo.send("同事二", "Nice to meet u too.");
	}
}
