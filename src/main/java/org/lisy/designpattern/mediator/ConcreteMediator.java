package org.lisy.designpattern.mediator;

/**
 * 具体中介类
 * - 知道所有的具体同事类
 * - 从具体同事类接收消息，向具体同事类发送信息
 * 
 * @author lisy
 */
public class ConcreteMediator extends Mediator {

    private ConcreteColleagueOne colleagueOne;
    
    private ConcreteColleagueTwo colleagueTwo;

    public void setColleagueOne(ConcreteColleagueOne colleagueOne) {
        this.colleagueOne = colleagueOne;
    }

    public void setColleagueTwo(ConcreteColleagueTwo colleagueTwo) {
        this.colleagueTwo = colleagueTwo;
    }

    @Override
    public void send(String message, Colleague colleague) {
        if(colleague == colleagueOne) {
        	colleagueTwo.notify(message);
        } else {
        	colleagueOne.notify(message);
        }
    }

}