package org.lisy.designpattern.mediator;

/**
 * 具体同事类
 * - 需要知道自己的行为和认识中介者
 * 
 * @author lisy
 */
public class ConcreteColleagueOne extends Colleague {

    public ConcreteColleagueOne(String name, Mediator mediator) {
        super(name, mediator);
    }

    public void send(String name, String message) {
    	System.out.println(name + " 发送消息：" + message);
        mediator.send(message, this);
    }

	public void notify(String message) {
        System.out.println(name + " 得到消息：" + message);
    }

}