package org.lisy.designpattern.mediator;

/**
 * 抽象中介类
 * - 优点：解耦
 * - 缺点：同事类多时中介类逻辑较复杂
 * 
 * @author lisy
 */
public abstract class Mediator {

    public abstract void send(String message, Colleague colleague);
}
