package org.lisy.designpattern.chainor;

/**
 * 具体处理类 - 部门经理
 * 
 * @author lisy
 */
public class DepartmentLeader extends Handler {

	@Override
	public void process(Integer info) {
		if (info > 3 && info <= 7) {
			System.out.println("department leader handler !");
		} else {
			nextHandler.process(info);
		}
	}

}
