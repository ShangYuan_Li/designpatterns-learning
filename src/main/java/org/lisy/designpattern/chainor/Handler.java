package org.lisy.designpattern.chainor;

/**
 * 抽象处理类
 * - 包含抽象处理方法
 * - 包含后继连接
 * 
 * @author lisy
 */
public abstract class Handler {

	protected Handler nextHandler;
	
	public void setNextHandler(Handler nextHandler) {
		this.nextHandler = nextHandler;
	}
	
	public abstract void process(Integer info);
	
}
