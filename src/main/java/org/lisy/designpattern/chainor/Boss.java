package org.lisy.designpattern.chainor;

/**
 * 具体处理类 - 老板
 * 
 * @author lisy
 */
public class Boss extends Handler {

	@Override
	public void process(Integer info) {
		if (info > 7) {
			System.out.println("boss handler !");
		} else {
			System.out.println("nobody handler !");
		}
	}

}
