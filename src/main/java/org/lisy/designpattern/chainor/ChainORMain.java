package org.lisy.designpattern.chainor;

/**
 * 责任链模式
 * - 抽象处理类
 * - 具体处理类(多个)
 * 优缺点:
 * - 优点：降低请求和处理耦合度，增强可扩展性，灵活
 * - 缺点：责任链较长时性能较低，不能保证请求处理
 * 
 * @author lisy
 */
public class ChainORMain {

	public static void main(String[] args) {
		
		// 创建处理链
		Handler level1 = new ProjectLeader();
		Handler level2 = new DepartmentLeader();
		level1.setNextHandler(level2);
		Handler level3 = new Boss();
		level2.setNextHandler(level3);
		
		// 向链头对象提交请求，无须关心请求的处理细节和请求的传递过程
		level1.process(10);
		level1.process(2);
		level1.process(6);
		level1.process(0);
	}
}

