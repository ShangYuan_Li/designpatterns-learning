package org.lisy.designpattern.chainor;

/**
 * 具体处理类 - 项目经理
 * - 实现抽象处理者的处理方法，判断能否处理本次请求
 * - 如果不能处理将请求转给后继者
 * 
 * @author lisy
 */
public class ProjectLeader extends Handler {

	@Override
	public void process(Integer info) {
		if (info > 0 && info <= 3) {
			System.out.println("project leader handler !");
		} else {
			nextHandler.process(info);
		}
	}

}
