# designpatterns-learning

#### 介绍
**设计模式学习记录**
1. 单例模式 - singleton
2. 策略模式 - strategy
3. 工厂方法 - factory method
4. 抽象工厂 - abstract factory
5. 外观模式 - facade
6. 中介者模式 - mediator
7. 责任链模式 - chain of responsibility
8. 装饰器模式 - decorator
9. 建造者模式 - builder
10. 桥接模式 - bridge
11. 适配器模式 - adapter
12. 模板方法 - template method
13. 状态模式 - state
14. 代理模式 - proxy
15. 命令模式 - command
16. 观察者模式 - observer
17. 享元模式 - flyweight
18. 组合模式 - composite
19. 原型模式 - prototype
20. 访问者模式 - visitor
21. 备忘录模式 - memento
22. 迭代器模式 - iterator
23. 解释器模式 - interpreter

**设计模式分类**
- *创建型模式* : singleton、factory method、abstract factory、builder、prototype
- *结构型模式* : adapter、bridge、composite、decorator、facade、flyweight、proxy
- *行为型模式* : chain of responsibility、command、interpreter、iterator、mediator、memento、observer、state、strategy、template method、visitor

**设计模式原则**
- 单一职责原则 (Single Responsibility Principle)
- 开闭原则 (Open·Closed Principle)
- 里氏替换原则 (Liscov Substitution Principle)
- 依赖倒置原则 (Dependency Inversion Principle)
- 接口隔离原则 (Interface Segregation Principle)
- 迪米特法则 (Law of Demeter)
